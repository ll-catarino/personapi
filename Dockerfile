FROM openjdk:15
COPY target/personAPI.jar personAPI.jar
#ENTRYPOINT ["java", "-jar", "personAPI.jar"]
CMD ["sleep", "5"] #wait for db to accept connections
CMD ["java", "-jar", "personAPI.jar"]
