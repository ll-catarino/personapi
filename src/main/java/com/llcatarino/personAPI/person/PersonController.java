package com.llcatarino.personAPI.person;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(path = "api/persons")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }


    @GetMapping
    @ApiOperation(value = "Gets every person", notes = "Returns every person and respective data", response = Person.class)
    public List<Person> getAllPersons() { return personService.getAllPersons(); }

    @GetMapping(path = "{personId}")
    @ApiOperation(value = "Gets a person by id", notes = "Returns a person and respective data", response = Person.class)
    public Person getPerson(@PathVariable("personId") Long personId) { return personService.getPerson(personId); }

    @PostMapping
    @ApiOperation(value = "Registers a new person", notes = "Add a person by POSTing a person object", response = Person.class)
    public void registerNewPerson(@RequestBody Person person) {
        personService.addNewPerson(person);
    }

    @DeleteMapping(path = "{personId}")
    @ApiOperation(value = "Deletes a person by id", notes = "Deletes a person by id", response = Person.class)
    public void deletePerson(@PathVariable("personId") Long personId) {
        personService.deletePerson(personId);
    }

    @PutMapping(path = "{personId}")
    @ApiOperation(value = "Update person details", notes = "Update any detail of person with id", response = Person.class)
    public void updatePerson(@PathVariable("personId") Long personId,
                             @RequestBody Person person) {
        personService.updatePerson(personId, person);
    }

}
