package com.llcatarino.personAPI.person;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

    private final PersonRepository personRepository;


    Logger logger = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAllPersons() {
        logger.trace("GET all persons");

        return personRepository.findAll();
    }

    public void addNewPerson(Person person) {
        logger.trace("POST new person");

        Optional<Person> personOptional = personRepository.findPersonByEmail(person.getEmail());

        if (personOptional.isPresent()) {
            logger.error("Error: Email already in use");
            throw new IllegalStateException("Error: Email already in use");
        }
        logger.trace("Success: Person added");
        personRepository.save(person);
    }

    public void deletePerson(Long personId) {
        logger.trace("DELETE person");
        boolean exists = personRepository.existsById(personId);

        if (!exists) {
            logger.error("Error: Person does not exist");
            throw new IllegalStateException("Error: Person does not exist");
        }
        logger.trace("Success: Person deleted");
        personRepository.deleteById(personId);
    }

    public Person getPerson(Long personId) {
        logger.trace("GET person");
        boolean exists = personRepository.existsById(personId);

        if (!exists) {
            logger.error("Error: Person does not exist");
            throw new IllegalStateException("Error: Person does not exist");
        }
        logger.trace("Success: Person found");
        return personRepository.findById(personId).get();
    }
    @Transactional
    public void updatePerson(Long personId, Person newPerson) {
        logger.trace("PUT person");

        Person person = personRepository.findById(personId).orElseThrow();

        if (newPerson.getName() != null && newPerson.getName().length() > 0) {
            person.setName(newPerson.getName());
        }
        
        if (newPerson.getSurname() != null && newPerson.getSurname().length() > 0) {
            person.setSurname(newPerson.getSurname());
        }
        
        if (newPerson.getOccupation() != null && newPerson.getOccupation().length() > 0) {
            person.setOccupation(newPerson.getOccupation());
        }
        
        if (newPerson.getDob() !=null) {
            person.setDob(newPerson.getDob());
        }
        
        if (newPerson.getHobbies() != null) {
            person.setHobbies(newPerson.getHobbies());
        }
        
        if (newPerson.getPassword() != null && newPerson.getPassword().length() > 0) {
            person.setPassword(newPerson.getPassword());
        }
        
        if (newPerson.getRoles() != null && newPerson.getRoles().length() > 0) {
            person.setRoles(newPerson.getRoles());
        }
        
        if (newPerson.getEmail() != null && newPerson.getEmail().length() > 0) {
            Optional<Person> personOptional = personRepository.findPersonByEmail(person.getEmail());

            if (personOptional.isPresent()) {
                logger.error("Error: Email already in use");
                throw new IllegalStateException("Error: Email already in use");
            }
            person.setEmail(newPerson.getEmail());
        }
        
        logger.trace("Success: Person details updated");
    }
}
