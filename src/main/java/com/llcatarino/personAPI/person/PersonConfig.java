package com.llcatarino.personAPI.person;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class PersonConfig {

    @Bean
    CommandLineRunner commandLineRunner(PersonRepository repository) {
        return args -> {
            Person bob = new Person(
                    "Bob",
                    "Bobson",
                    "carpenter",
                    LocalDate.of(2000, Month.OCTOBER, 21),
                    "bob@mail.com",
                    "$2y$10$FZfux7vZTYrinDdDfQXILO.Hpauw1w.lVi07MHJd/dqw33/I7PHaW",
                    List.of("programming", "baseball"),
                    "ADMIN, USER");

            Person carl = new Person(
                    "Carl",
                    "Carlson",
                    "mechanic",
                    LocalDate.of(2000, Month.APRIL, 1),
                    "carl@mail.com",
                    "$2y$10$FZfux7vZTYrinDdDfQXILO.Hpauw1w.lVi07MHJd/dqw33/I7PHaW",
                    List.of("baseball", "barbecue"),
                    "USER");

            repository.saveAll(List.of(bob, carl));
        };
    }

}
