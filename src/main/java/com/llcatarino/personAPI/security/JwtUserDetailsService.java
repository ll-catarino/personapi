package com.llcatarino.personAPI.security;

import com.llcatarino.personAPI.person.Person;
import com.llcatarino.personAPI.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    PersonRepository personRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Person person = personRepository
                        .findPersonByEmail(email)
                        .orElseThrow(() -> new UsernameNotFoundException("Error: user not found"));


        return new JwtUserDetails(person);

    }
}
